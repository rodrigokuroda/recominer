package br.edu.utfpr.recominer.metric.committer;

/**
 *
 * @author Rodrigo T. Kuroda
 */
public final class EmptyCommitterFileMetrics extends CommitterFileMetrics {

    public EmptyCommitterFileMetrics() {
        super(null, null, 0.0, 0.0);
    }

}
